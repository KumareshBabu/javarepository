package com.kumaresh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.groupchat.connection.DBConnection;

public class CommentDaoImpl implements CommentDao{

	@Override
	public void insertComment(String insertComment) {
		// TODO Auto-generated method stub
		try{
			String SQL = "insert into sn values(?)";
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement(SQL);
			ps.setString(1,insertComment);
			ps.executeUpdate();
		}
		catch(Exception e){
			System.out.println("Exception is " + e);
		}
	}

	@Override
	public List<String> fetchComments() {
		// TODO Auto-generated method stub
		List<String> comments = new ArrayList<String>();
		try{
			String SQL = "select comment from sn";
			Connection con1 = DBConnection.getConnection();
			PreparedStatement ps = con1.prepareStatement(SQL);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				comments.add(rs.getString("comment"));
			}
			return comments;
		}
		catch(Exception e){
			System.out.println("Select Query Exception is " + e);
		}
		return null;
	}

}
