package com.groupchat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.groupchat.model.User;
import com.groupchat.register.dao.AddUser;

@Controller
public class RegisterController {
	
	@Autowired
	private AddUser addUser;
	
	@RequestMapping(value="/actionRegister", method=RequestMethod.POST)
	public String registerUser(@ModelAttribute("modelForm") User user, ModelMap map){
		
		addUser.addUser(user);
		addUser.sendMail(user);
		
		map.addAttribute("name", user.getFirstName()+" "+user.getLastName());
		
		return "login";
	}
}
