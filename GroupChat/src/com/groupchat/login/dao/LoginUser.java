package com.groupchat.login.dao;

import com.groupchat.model.LoginUserModel;

public interface LoginUser {

	public boolean checkLogin(LoginUserModel loginUserModel);
	
}
