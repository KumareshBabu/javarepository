package com.groupchat.login.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.groupchat.connection.DBConnection;
import com.groupchat.model.LoginUserModel;

public class LoginUserImpl implements LoginUser{

	@Override
	public boolean checkLogin(LoginUserModel loginUserModel) {
		// TODO Auto-generated method stub
		try{
			Connection con = DBConnection.getConnection();
			String SQL = "select count(*) from groupchat where emailid=? and password=?";
			PreparedStatement ps = con.prepareStatement(SQL);
			ps.setString(1,loginUserModel.getEmailId());
			ps.setString(2,loginUserModel.getPassword());
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				int count = rs.getInt(1);
				if(count > 0)
					return true;
				else
					return false;
			}
		}
		catch(Exception e){
			System.out.println("Query Login Exception is " + e);
		}
		return false;
	}

}
