package com.groupchat.register.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.github.sendgrid.SendGrid;
import com.groupchat.connection.DBConnection;
import com.groupchat.connection.SendGridConnection;
import com.groupchat.model.User;

public class AddUserImpl implements AddUser{

	@Override
	public void addUser(User user) {
		// TODO Auto-generated method stub
		try{
			Connection con = DBConnection.getConnection();
			boolean checkConn = con.isReadOnly();
			System.out.println("CheckConnection is " + checkConn);		
			String SQL = "insert into groupchat values(?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(SQL);
			ps.setString(1,user.getFirstName());
			ps.setString(2,user.getLastName());
			ps.setString(3, user.getEmailId());
			ps.setString(4, user.getPassword());
			ps.executeUpdate();
		}
		catch(Exception e){
			System.out.println("Insert query exception is " + e);
		}
	}

	@Override
	public void sendMail(User user) {
		// TODO Auto-generated method stub
		
		SendGrid sendGrid = SendGridConnection.getSendGridConnection();
		sendGrid.addTo(user.getEmailId());
		sendGrid.setFrom("kumareshcs20@outlook.com");
		sendGrid.setSubject("Registration Completed for GroupChat!");
		sendGrid.setText("Hi " + user.getFirstName()+" "+user.getLastName()+","+"\n"+"\n"
		+"You are successfully registered to groupchat. Try GroupChat and connect with friends."
		+"\n"+"\n"
		+ "\n"+"GroupChat Team.");
		sendGrid.send();
	}

}
