package com.groupchat.register.dao;

import com.groupchat.model.User;

public interface AddUser {

	public void addUser(User user);

	public void sendMail(User user);
	
}
