<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <meta http-equiv="refresh" content="5" > -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<title>Group Chat</title>
</head>
<body>
<center><br/><br/>

        <table>
        	<c:forTokens items="${userComment}" delims="," var="comment" >
        	<tr>
        		<td>
        				<b><c:out value="${comment}"/></b>
        		</td>
        	</tr>
        </c:forTokens>
        </table><br/><br/>
        
         <form:form action="commentAction" modelAttribute="commentModel" method="post">
        	<textarea rows="10" cols="50" id="commentPlace" name="userComment" placeholder="Type your message"></textarea><br/><br/>
        	<input type="submit" class="btn btn-success" value="Post" /> &nbsp;&nbsp;
        </form:form><br/>
       <%@include file="refresh.jsp" %>
        </center>
</body>
</html>