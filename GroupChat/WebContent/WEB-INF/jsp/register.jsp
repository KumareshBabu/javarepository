<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>
</head>
<body>
<center>
	<h2>Registration for Group Chat</h2><br/><br/>
	<form:form name="registerForm" modelAttribute="modelForm" action="actionRegister" method="post">
		<input type="text" name="firstName" placeholder="First Name"/><br/><br/>
		<input type="text" name="lastName" placeholder="Last Name" /><br/><br/>
		<input type="text" name="emailId" placeholder="Email Id" /><br/><br/>
		<input type="password" name="password" placeholder="Password" /><br/><br/>
		<input type="submit" value="Register" /><br/><br/>
	</form:form>
	<a href="login"><h3>Login Here!</h3></a>
</center>
</body>
</html>